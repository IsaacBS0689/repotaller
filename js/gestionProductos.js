var productosObtenidos;

function getProductos()
{
 var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
 var request = new XMLHttpRequest();
 request.onreadystatechange = function (){
   if(this.readyState == 4 && this.status == 200){
     //console.log(request.responseText);
     productosObtenidos = request.responseText;
     procesarProductos();
   }
 }
 request.open("GET", url, true);
 request.send();
}
function procesarProductos () {
 var JSONProductos = JSON.parse(productosObtenidos);
 //alert(JSONProductos.value[0].ProductName);
 //for (var i = 0; i < JSONProductos.value.length; i++) {
 //  console.log(JSONProductos.value[i].ProductName);
 //}
 var divTabla = document.getElementById("divTabla");
 var tabla = document.createElement("table");
 var tbody = document.createElement("tbody");
 tabla.classList.add("table");
 tabla.classList.add("table-striped");

 for (var i = 0; i < JSONProductos.value.length; i++) {
   var nuevaFila = document.createElement("tr");

   var coulumnaNombre = document.createElement("td");
   coulumnaNombre.innerText = JSONProductos.value[i].ProductName;

   var coulumnaPrecio = document.createElement("td");
   coulumnaPrecio.innerText = JSONProductos.value[i].UnitPrice;

   var coulumnaStock = document.createElement("td");
   coulumnaStock.innerText = JSONProductos.value[i].UnitsInStock;

   nuevaFila.append(coulumnaNombre);
   nuevaFila.append(coulumnaPrecio);
   nuevaFila.append(coulumnaStock);

   tbody.appendChild(nuevaFila);

//   tabla.appendChild(nuevaFila);
 }
tabla.appendChild(tbody);
 divTabla.append(tabla);
}
